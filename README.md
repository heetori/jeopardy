# Jeopardy

Schulz William Solignac Gauthier

## Présentation de Jeopardy

Le jeu consiste a répondre à des questions sur plusieurs thèmes.
Les questions seront affichées sous forme d'un tableau où les colonnes sont les catégories
et les lignes sont la difficulté croissante de bas en haut. 
Les joueurs ne voient que les points qui seront remportés en cas de bonne réponse.
Exemple : 
+----------+------------+-------+
| Histoire | Géographie | Maths |
+----------+------------+-------+
|       10 |         10 |    10 |
|       20 |         20 |    20 |
|       30 |          - |    30 |
|       40 |         40 |    40 |
|       50 |         50 |     - |
+----------+------------+-------+
Dans ce cas la question à 50 points de maths et celle à 30 points de Géographie ont été choisies.

Il se joue l'un après l'autre.
Le joueur en cours choisit la catégorie et le nombre de points et si il répond avec succès, il gagne le nombre de points.
Ensuite la main va au joueur suivant.

## Utilisation de Jeopardy

Afin d'utiliser le projet, il doit être suffisant de taper les 
commandes suivantes:
./compile.sh            // lancer la compilation des fichiers
                        // présents dans 'src' et création des 
                        // fichiers '.class' dans 'classes'

Comme certains binômes ont un projet en mode texte et un autre 
en mode graphique, merci de préciser le nom des programmes à 
passer en paramètre au script 'run.sh'

./run.sh Jeopardy    (mode texte)
./run.sh <NomDuProgrammeGraphique> (mode graphique si présent)

Pour tous les projets: pensez à mettre l'ap.jar dans le répertoire 'lib' !

Pour les projets en mode graphiques, placez les images dans le répertoire
'ressources' sachant que son contenu est copié dans 'classes' lorsque 
vous faites un 'run'.

