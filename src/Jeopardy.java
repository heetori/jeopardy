/*

Jeopardy V0.0.1

Schulz William Solignac Gauthier

Version 0.0.1 :
	- Implémentation d'un menu dynamique
	- Gestion de la séléction du menu
	- Fonction de démarrage de la partie


Les questions sont sauvagardées dans plusieurs fichiers CSV qui contiendront chaque catégorie de questions

*/

/*anglais 30
mat 40 mat 50 fr 50*/

import extensions.CSVFile;

class Jeopardy extends Program{

	final int NB_MATIERES 					= 5; // Pourrait changer si on ajoute d'autres matieres
	final int STATE_MENU       			= 1;
	final int STATE_GAME       			= 2;
	final int STATE_OPTIONS    			= 3;
	final int STATE_REP        			= 4;
	final int STATE_FINISHED   			= 5;
	int state 											= STATE_MENU; // La variable contient l'état global du programme (en jeu, dans le menu, etc)
	final String FICHIERQUESTIONS 	= "questions.csv";
	boolean locked 									= false;
	boolean gameStart 							= false;
	Position pos 										= creerPos(); // position dans le tableau du menu du jeu
	Matieres mat 										= creerMatieres();
	Question questEncours;
	Joueurs joueurs;

	//Créée une position pour les différents menus
	Position creerPos(){
		Position p 	= new Position();
		p.x 				= 0;
		p.y 				= 0;
		p.max_x 		= 1;
		p.max_y 		= 1;
		return p;
	}

	//Créée une matière
	Matieres creerMatieres(){
		Matieres matieres = new Matieres();
		matieres.tab = new Matiere[NB_MATIERES];
		matieres.nb = 0;
		return matieres;
	}

	// Créée une question
	Question creerQuestion(String txt, String[] reponses, String repCorrecte, int nbPts){
		Question q = new Question();
		q.txt = txt;
		q.reponses = reponses;
		q.repCorrecte = repCorrecte;
		q.suivante = null;
		q.nbPts = nbPts;
		return q;
	}

	// Créée un joueur
	Joueur creerJoueur(String nom){
		Joueur j = new Joueur();
		j.nom = nom;
		j.pts = 0;
		return j;
	}

	// Créée la liste des joueurs
	Joueurs creerJoueurs(int nb){
		Joueurs joueurs = new Joueurs();
		joueurs.joueurs = new Joueur[nb];
		joueurs.nb_joueurs = nb;
		joueurs.joueurActuel = 0;
		return joueurs;
	}

/*
Les commentaires suivant fonctionne de la façon suivante :
	Les commentaires pouvant contenir plusieurs lignes seront des changement de partie
	Les commenaitres ne pouvant contenir qu'une seul ligne servent d'indicateur pour élcairé le programme
*/

	void algorithm(){
		chargerQuestions(mat);
		hide(); // On cache le curseur
		enableKeyTypedInConsole(true);
		while(true){
			switch(state){
				case STATE_MENU:
					menu();
					break;
				case STATE_GAME:
					game();
					break;
				case STATE_OPTIONS:
					afficherScoreboard();
					break;
				case STATE_REP:
					rep();
					break;
				case STATE_FINISHED:
					score_fini();
					break;
			}
			while(locked){
				delay(50); // Le delay est necessaire pour pouvoir quitter la boucle
			}
		}
	}

	/*
	keyTypedInConsole :
	Cette fonction permet d'implémenter les touches du clavier
	Nous activons et désaction a notre guise les touhces du clavier
	Cela nous permet entre autre de ne pas faire buguer
	la console lors de l'appel de la fonction quitter
	*/

	void keyTypedInConsole(char c){
		switch (state){
			case STATE_MENU:
				keyTypedInMenu(c);
				break;
			case STATE_GAME:
				keyTypedInGame(c);
				break;
			case STATE_OPTIONS:
				keyTypedInOptions(c);
				break;
			case STATE_REP:
				keyTypedInRep(c);
				break;
			case STATE_FINISHED:
				keyTypedInFinished(c);
				break;
		}
	}

	/* Multiples fonctions de la gestion de la position du curseur dans les menus*/

	void goUp(Position pos){
		pos.x--;
		if(pos.x < 0){
			pos.x = pos.max_x - 1;
		}
	}

	void goDown(Position pos){
		pos.x++;
		pos.x = pos.x % pos.max_x;
	}

	void goRight(Position pos){
		pos.y++;
		pos.y = pos.y % pos.max_y;
	}

	void goLeft(Position pos){
		pos.y--;
		if(pos.y < 0){
			pos.y = pos.max_y - 1;
		}
	}

	/*Controle de la fin de la partie*/

	void score_fini(){
		state = STATE_FINISHED;
		locked = true;
		clearScreen();
		cursor(0,0);
		afficherScore();
	}

	void keyTypedInFinished(char c){
		state = STATE_MENU;
		locked = false;
	}

	boolean fini(){
		for(int i = 0; i < NB_MATIERES;i++){
			Matiere m = mat.tab[i];
			for(int j = 0; j < length(m.repondu); j++){
				if(!m.repondu[j]){
					return false;
				}
			}
		}
		return true;
	}

	/*
	Affichage et controle du menu générale:
	Afficher menu comme son nom l'indique permet d'afficher
	le menu en fonction de la variable pos
	modifier par l'appel de la touche AINSI UP ou AINSI DOWN
	*/

void menu(){
		locked = true; // On bloque l'exécution du programme
		pos.x = 0;
		pos.y = 0;
		pos.max_x = 3;
		pos.max_y = 1;
		state = STATE_MENU;
		afficherMenu(pos);
	}

	void afficherMenu(Position pos){
		clearScreen();
		cursor(0,0);
		hide();
		switch(pos.x){
			case 0:
			print(ANSI_RED);
			cursor(1,0);
			println(">Jouer");
			print(ANSI_TEXT_DEFAULT_COLOR);
			cursor(2,0);
			println("Afficher le tableau des scores");
			cursor(3,0);
			println("Quitter");
			break;
			case 1:
			cursor(1,0);
			println("Jouer");
			print(ANSI_RED);
			cursor(2,0);
			println(">Afficher le tableau des scores");
			print(ANSI_TEXT_DEFAULT_COLOR);
			cursor(3,0);
			println("Quitter");
			break;
			case 2:
			cursor(1,0);
			println("Jouer");
			cursor(2,0);
			println("Afficher le tableau des scores");
			print(ANSI_RED);
			cursor(3,0);
			println(">Quitter");
			print(ANSI_TEXT_DEFAULT_COLOR);
			break;
		}
	}

	void keyTypedInMenu(char c){ // Quand une touche a été appuyée dans le menu
		switch(c){
			case ANSI_UP:
			goUp(pos);
			break;
			case ANSI_DOWN:
			goDown(pos);
			break;
			case (char)13: // Entrée
			choiceMenu(pos);
			break;
		}
		afficherMenu(pos);
	}

	void choiceMenu(Position pos){
		switch(pos.x){
			case 0:
			state = STATE_GAME;
			locked = false;
			break;
			case 1:
			state = STATE_OPTIONS;
			locked = false;
			break;
			case 2:
			exit();
			break;
		}
	}

/* Affichage et controle du menu en jeu */

	void game(){
		if(!gameStart){
			show();
			enableKeyTypedInConsole(false);
			println("Combien de joueurs ? ");
			boolean is_ok = 0!=0;
			int nb_joueurs = lectureInt(25);

			joueurs = creerJoueurs(nb_joueurs);
			for(int i=0; i<joueurs.nb_joueurs; i++){
				println("Nom du joueur " + (i+1));
				joueurs.joueurs[i] = creerJoueur(readString());
			}
			gameStart = true;
			enableKeyTypedInConsole(true);
		}
		locked = true; // On bloque l'exécution du programme
		pos.x = 0;
		pos.y = 0;
		pos.max_x = 5;
		pos.max_y = NB_MATIERES;
		state = STATE_GAME;
		afficherGame(pos);
	}

	void afficherGame(Position pos){
		delay(10);
		clearScreen();
		cursor(0,0);
		print("Au tour de : " + getPlayerName(joueurs.joueurActuel));
		cursor(2,0);
		int val = 50; // Nombre de point max
		for(int i = 0; i < length(mat.tab);i ++) {
			print(mat.tab[i].nom + " ");
		}
		for(int j = 5;j >= 1;j--){
			cursor(j+2,0);
			for(int k = 0; k < NB_MATIERES;k++){
				int espace_avant = (length(mat.tab[k].nom)/2)-1;
				int espace_après = length(mat.tab[k].nom) - espace_avant-1;
				if(pos.x == (j-1) && pos.y == k){
					print(ANSI_RED);
				}
				for(int e = 0; e < espace_avant; e++){
					print(" ");
				}
				if(!mat.tab[k].repondu[j-1]){
					print(j*10);
				}else{
					print("--");
				}
				for(int e = 0; e < espace_après; e++){
					print(" ");
				}
				print(ANSI_TEXT_DEFAULT_COLOR);
			}
		}
		cursor(8,0);
		afficherScore();
	}

	void afficherScore(){ //Affiche le score des joueurs en dessous du tableau
		String j =" | ";
		for(int i =0; i < joueurs.nb_joueurs;i++){
			j = j + "Score de " + getPlayerName(i) + " : " + getPlayerScore(i) + " | ";
		}
		print(j);
	}

	void keyTypedInGame(char c){ // Quand une touche a été appuyée en jeu
		if(c == ANSI_UP){
			goUp(pos);
		}
		if(c == ANSI_DOWN){
			goDown(pos);
		}
		if(c == (char)20){
			goRight(pos);
		}
		if(c == (char)19){
			goLeft(pos);
		}
		if(c == (char)13){
			veriferPasRepondu();
		}
		if(c == (char)99){
			exit();
		}
		afficherGame(pos);
	}

	int lectureInt(int max){
		int nb_joueurs = readInt();
		while(true){
			if(nb_joueurs <= 0){
				println("Il doit y a voir au moins un joueur");
			}else if (nb_joueurs >= 25){
				println("Il ne peut pas y avoir plus de " + max + " joueurs");
			} else {
				return nb_joueurs;
			}
			nb_joueurs = readInt();
		}
	}

	void veriferPasRepondu(){
		if(!mat.tab[pos.y].repondu[pos.x]){
			state = STATE_REP;
			locked = false;
		}
	}

/* Affichage et controle du menu des réponses */

	void rep(){
		state = STATE_REP; // Pour être sûr que quand nous exécutons cette fonction l'état est bien celui attendu
		locked = true;
		questEncours = getRandomQuestion(mat.tab[pos.y], (pos.x+1)*10);
		pos.x = 0;
		pos.y = 0;
		pos.max_y = 1;
		pos.max_x = length(questEncours.reponses);
		afficherQuestionEtReponse();
	}

	void afficherQuestionEtReponse(){
		clearScreen();
		cursor(0,0);
		println(questEncours.txt);
		for(int i = 0; i < length(questEncours.reponses); i++){
			if( pos.x == i){
				print(ANSI_RED);
				print(questEncours.reponses[i] + " ");
				print( ANSI_TEXT_DEFAULT_COLOR);
			} else {
				print(questEncours.reponses[i] + " ");
			}
		}
	}

	void keyTypedInRep(char c){
		switch(c){
			case (char)19:
			goUp(pos);
			break;
			case (char)20:
			goDown(pos);
			break;
			case (char)13:
			choiceRep();
			break;
			case (char)99:
			exit();
			break;
		}
		afficherQuestionEtReponse();
	}

	void choiceRep(){
		if(equals(questEncours.reponses[pos.x], questEncours.repCorrecte)){
			ajouterPtsJoueurActuel(questEncours.nbPts);
			clearScreen();
			cursor(0,0);
			print("Bien joué !");
			delay(1000);
			state = STATE_GAME;
			locked = false;
		} else {
			cursor(0,0);
			clearScreen();
			print("Raté");
			delay(1000);
			state = STATE_GAME;
			locked = false;
		}
		passerJoueurSuivant();
		if(fini()){
			state = STATE_FINISHED;
			locked = false;
			saveScore();
			gameStart = false;
		}
	}

	/*Affiche le tableau des scores enregistrer dans un fichier csv et gestion des scores*/
	void afficherScoreboard(){
		clearScreen();
		int tmp = 0;
		cursor(0,0);
		locked = true;
		CSVFile score = loadCSV("../score.csv");
		for(int i = 0; i < rowCount(score); i++){
			cursor(i+1,0);
			for(int j = 0; j < columnCount(score); j++){
				if(j == 0){
					print("Nom : ");
				} else {
					print(" | Score : ");
				}
				print(getCell(score, i, j));
			}
			tmp = i;
		}
		cursor(tmp+2,0);
		print("Appuyer sur entrée pour quitter");
	}

	void keyTypedInOptions(char c){
		if(c == (char)13){
			state = STATE_MENU;
			locked = false;
		}
	}

	void saveScore(){
		for(int k = 0; k < joueurs.nb_joueurs; k++){
			String nom = getPlayerName(k);
			int pts = getPlayerScore(k);
			CSVFile score = loadCSV("../score.csv");
			String [][] tmp;
			if(rowCount(score) == 0){
				tmp = new String [1][2];
			} else {
				tmp = new String [rowCount(score)+1][columnCount(score)];
				for(int i = 0; i < rowCount(score); i++){
					for(int j = 0; j < columnCount(score); j++){
						tmp[i][j] = getCell(score, i, j);
					}
				}
			}
			if(rowCount(score) == 0){
				tmp[0][0] = nom;
				tmp[0][1] = pts + "";
			} else {
				tmp[rowCount(score)][0] = nom;
				tmp[rowCount(score)][1] = pts + "";
			}
			//String tmp = nom + "," + pts;
			saveCSV(tmp, "../score.csv");
		}
	}

	/* Gestion des joueurs */

	void passerJoueurSuivant(){
		joueurs.joueurActuel++;
		if(joueurs.joueurActuel >= joueurs.nb_joueurs){
			joueurs.joueurActuel = 0;
		}
	}

	String getPlayerName(int id){
		return joueurs.joueurs[id].nom;
	}

	int getPlayerScore(int id){
		return joueurs.joueurs[id].pts;
	}

	void ajouterPtsJoueurActuel(int nbPts){
		joueurs.joueurs[joueurs.joueurActuel].pts = joueurs.joueurs[joueurs.joueurActuel].pts + nbPts;
	}

	/*
	La fonction exit ne ne consiste qu'a quitter le programme
	lors de la séléction dans le menu du bouton quitter
	*/

	void exit(){
		show();
		enableKeyTypedInConsole(false);
		reset();
		cursor(0,0);
		System.exit(0);
	}

	/* Gestion des Matières */

	Matiere ajouterMatiere(String nom){
		Matiere m = new Matiere();
		m.nom = nom;
		m.repondu = new Boolean[]{false, false, false, false, false};
		return m;
	}

	// Renvoie l'index de la matière ayant comme nom s, renvoie -1 sinon
	int findMatiere(String s, Matieres matieres){
		for(int i=0; i<matieres.nb; i++){
			if(equals(s, matieres.tab[i].nom)){
				return i;
			}
		}
		return -1;
	}

	/* Gestion des Questions */

	void chargerQuestions(Matieres matieres){
		CSVFile fichier = loadCSV(FICHIERQUESTIONS);
		for(int i=0; i<rowCount(fichier); i++){
			String nomMat = getCell(fichier, i, 0);
			int iMat = findMatiere(nomMat, matieres);
			if(iMat == -1){ // Si la matière n'existe pas encore dans le tableau
				matieres.tab[matieres.nb] = ajouterMatiere(nomMat);
				iMat = matieres.nb;
				matieres.nb++;
			}
			int nbPts = stringToInt(getCell(fichier, i, 1));
			String txt = getCell(fichier, i, 2);
			String txt_reponses = getCell(fichier, i, 3);
			String[] reponses = processReponses(txt_reponses);
			String repCorrecte = getCell(fichier, i, 4);
			switch(nbPts){
				case 50:
					matieres.tab[iMat].q50pts = ajouterQuestion(matieres.tab[iMat].q50pts, txt, reponses, repCorrecte, nbPts);
					break;
				case 40:
					matieres.tab[iMat].q40pts = ajouterQuestion(matieres.tab[iMat].q40pts, txt, reponses, repCorrecte, nbPts);
					break;
				case 30:
					matieres.tab[iMat].q30pts = ajouterQuestion(matieres.tab[iMat].q30pts, txt, reponses, repCorrecte, nbPts);
					break;
				case 20:
					matieres.tab[iMat].q20pts = ajouterQuestion(matieres.tab[iMat].q20pts, txt, reponses, repCorrecte, nbPts);
					break;
				case 10:
					matieres.tab[iMat].q10pts = ajouterQuestion(matieres.tab[iMat].q10pts, txt, reponses, repCorrecte, nbPts);
					break;
				default:
					println("Erreur lors du chargement des matières");
					exit();
			}
		}
		if(matieres.nb != NB_MATIERES){
			println("Il manque des matières dans le fichier csv");
			exit();
		}
	}

	// Ajoute une question à la liste chainée ou créée la liste si q est vide
	Question ajouterQuestion(Question q, String txt, String[] reponses, String repCorrecte, int nbPts){
		if(q == null){ // Si q n'existe pas
			q = creerQuestion(txt, reponses, repCorrecte, nbPts);
		}else{
			q = dernierElem(q); // On va à la fin de la liste chainée
			q.suivante = creerQuestion(txt,  reponses, repCorrecte, nbPts);
		}
		return q;
	}

	// Retourne la question à l'indice i
	Question getQuestion(Question q, int i){
		if(length(q) < i){ // Si il n'y a pas de question à l'indice i
			println("Pas de question à l'indice i");
			exit();
			return null;
		}
		for(int x = 0;  x < i; x++){
			q = q.suivante;
		}
		return q;
	}

	// Retourne une question au hasard de la matière mat avec nbPts points
	Question getRandomQuestion(Matiere mat, int nbPts){
		mat.repondu[(nbPts/10)-1] = true;
		Question q = getQuestion(mat, nbPts);
		int indice = (int) (random()*length(q));
		return getQuestion(q, indice);
	}

	// Retourne la dernière question de la liste
	Question dernierElem(Question q){
		if(q.suivante == null){
			return q;
		}
		return dernierElem(q.suivante);
	}

	int length(Question q){
		if(q == null){
			return 0;
		}
		if(q.suivante == null){
			return 1;
		}
		return 1+length(q.suivante);
	}

	void testQuestions(){
		String[] rep = new String[] {"AZDE", "QSD", "QSFB", "YH"};
		Question q = creerQuestion("1", rep, "AZDE", 10);
		assertEquals(length(q), 1);
		// assertEquals(q.suivante, null);
		assertEquals(q.txt, "1");
		assertArrayEquals(q.reponses, rep);
		assertEquals(q.repCorrecte, "AZDE");
		ajouterQuestion(q, "12", rep, "rep", 10);
		assertTrue(q.suivante == dernierElem(q));
		Question q2 = null;
		assertEquals(length(q2), 0);
		q2 = ajouterQuestion(q2, "A", rep, "C", 30);
	}

	String[] processReponses(String s){
		int indiceslash = 1; // On commence à 1 car les réponses sont stockées sous la forme aze/rty/uio
		// il y a donc 2 slashs pour 3 réponses
		for(int i = 0; i < length(s); i++){
			if(charAt(s,i) == '/'){
				indiceslash++;
			}
		}
		int tmp = 0;
		String[] tab = new String[indiceslash];
		for(int i = 0; i < length(s); i++){
			if(charAt(s,i) == '/'){
				tmp++;
			} else {
				tab[tmp] = ((tab[tmp] == null)?"":tab[tmp]) + charAt(s,i);
			}
		}
		return tab;
	}

	Question getQuestion(Matiere mat, int nbPts){
		Question q;
		switch(nbPts){
			case 50:
				q = mat.q50pts;
				break;
			case 40:
				q = mat.q40pts;
				break;
			case 30:
				q = mat.q30pts;
				break;
			case 20:
				q = mat.q20pts;
				break;
			case 10:
				q = mat.q10pts;
				break;
			default:
				q = null; // Pour que le compileur soit content
				println("Erreur pas de points correspondant");
				exit();
		}
		return q;
	}
}
